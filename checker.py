# vim: noet ai ts=4 sts=4 sw=4

import subprocess
import re

class PABTChecker:
	def __init__(self, pabt):
		self.pabt = pabt
		self.log = pabt.log

	def check(self):
		self.check_card()
		self.check_sink_status()
		self.check_default_sink()
		self.check_card_profile()

	def find_card(self):
		args = ["pactl list cards"]
		o = subprocess.check_output(args, shell=True, stderr=subprocess.STDOUT)
		card = None
		want_name_line = "\tName: " + self.pabt.card_name()
		for line in o.decode("utf-8").splitlines():
			if line.find("Card #") == 0 and card is not None:
				break
			if line.find(want_name_line) == 0:
				card = []
				continue
			if card is None:
				continue
			card.append(line)
		return card

	def check_card(self):
		card = self.find_card()
		if card is None:
			self.log(1, "ERROR - No card found")
			return False
		found_desc = False
		for line in card:
			m = re.search('device.description = "(.*)"', line)
			if m is not None:
				self.log(1, "OK - Card found: " + self.pabt.card_name() + \
						" (" + m[1] + ")")
				found_desc = True
		if not found_desc:
			self.log(1, "OK - Card found: " + self.card_name())
		return True

	def check_sink_status(self):
		args = ["pactl list short"]
		o = subprocess.check_output(args, shell=True, stderr=subprocess.STDOUT)
		for line in o.decode("utf-8").splitlines():
			if self.pabt.sink_name() + "\t" not in line:
				continue
			# SUSPENDED
			if "RUNNING" in line:
				self.log(1, "OK - Sink found: " + self.pabt.sink_name())
				return True
			elif "IDLE" in line:
				self.log(1, "OK - Sink is idle: " + self.pabt.sink_name())
				return True
			else:
				self.log(1, "WARN - Sink exists: " + line)
				return False
		self.log(1, "ERROR - Sink not detected")
		return False

	def check_default_sink(self):
		args = ["pactl info"]
		o = subprocess.check_output(args, shell=True, stderr=subprocess.STDOUT)
		for line in o.decode("utf-8").splitlines():
			m = re.search("^Default Sink: (.*)", line)
			if m is None:
				continue
			sink = m[1]
			if sink == self.pabt.sink_name():
				self.log(1, "OK - Default sink: " + sink)
				return True
			elif sink == self.pabt.sink_name_base():
				self.log(1, "WARN - Default sink with bad profile: " + sink)
				return False
			else:
				self.log(1, "ERROR - Default sink: " + sink)
				return False
		print("ERROR - No default sink detected")
		return False

	def check_card_profile(self):
		card = self.find_card()
		if card is None:
			return False
		self.check_available_profiles(card)
		self.check_active_profile(card)

	def check_available_profiles(self, card):
		in_profile = True
		other_profiles = []
		for line in card:
			m = re.search("^\t(\w+):", line)
			if m is not None:
				in_profile = m[1] == "Profiles"
				continue
			if not in_profile:
				continue
			m = re.search("\t(\w+): (.*?) \(sinks:.*available: (\w+)\)", line)
			if m is None:
				continue
			if m[1] == self.pabt.profile:
				if m[3] == "yes":
					self.log(1, "OK - Profile found: " + m[1] + " " + m[2])
					return True
				else:
					other_profiles.append((m[1], m[2], m[3]))
			else:
				other_profiles.append((m[1], m[2], m[3]))
		for name, desc, avail in other_profiles:
			if name == self.pabt.profile:
				self.log(1, "ERROR - Profile found but not available: " + \
						name + " / " + desc + " / " + avail)
		for name, desc, avail in other_profiles:
			if name != self.pabt.profile:
				self.log(1, "INFO - Profile found: " +
						name + " / " + desc + " / " + avail)

	def check_active_profile(self, card):
		for line in card:
			m = re.search("Active Profile: (.*)", line)
			if m is None:
				continue
			if m[1] == self.pabt.profile:
				self.log(1, "OK - Active profile: " + m[1])
				return True
			else:
				self.log(1, "WARN - Active profile: " + m[1])
				return False
		self.log(1, "ERROR - Active profile not found")
		return False
