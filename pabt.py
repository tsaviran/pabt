#! /usr/bin/env python3
# vim: noet ai ts=4 sts=4 sw=4

# requires python3-pydbus on Debian

from pydbus import SystemBus
from gi.repository import GLib
import subprocess
import sys
import argparse
from checker import PABTChecker
from fixer import PABTFixer


class PABT:
	def __init__(self, mac, profile="a2dp_sink", verbose=0):
		self.mac = mac
		self.loop = GLib.MainLoop()
		self.bus = SystemBus()
		name = "org.bluez"
		path = "/org/bluez/hci0/dev_" + self.mac_path()
		self.dev = self.bus.get(name, path)
		self.mac = mac
		self.profile = profile
		self.verbose = verbose

		self.dev.onPropertiesChanged = self.on_properties_changed

	def run(self):
		if self.verbose >= 1:
			print("Started with MAC " + self.mac)
		self.loop.run()

	def log(self, level, msg):
		if self.verbose >= level:
			print(msg)

	def mac_path(self):
		return self.mac.replace(":", "_")

	def card_name(self):
		return "bluez_card." + self.mac_path()

	def sink_name_base(self):
		return "bluez_sink." + self.mac_path() + "."

	def sink_name(self):
		return self.sink_name_base() + self.profile

	def on_properties_changed(self, service, changed, invalidated):
		self.log(3, "Event: service = " + service +
					", changed = " + str(changed) +
					", invalidated = " + str(invalidated))
		if service.startswith("org.bluez.Device"):
			if changed.get("Connected"):
				self.connect_bluetooth()
			if changed.get("ServicesResolved"):
				self.pulseaudio_set_card_profile()

	def connect_bluetooth(self):
		self.log(1, "Connecting to " + self.mac)
		self.call_bluetoothctl(["connect", self.mac])

	def disconnect_bluetooth(self):
		self.log(1, "Disconnecting " + self.mac)
		self.call_bluetoothctl(["disconnect", self.mac])

	def pulseaudio_set_card_profile(self):
		self.log(1, "Setting profile of MAC " + self.mac +
					" to " + self.profile)
		args = ["pacmd", "set-card-profile", self.card_name(), self.profile]
		self.log(2, "Exec: " + str(args))
		subprocess.call(args)

	def call_bluetoothctl(self, command):
		p1_args = ["echo"] + command
		p2_args = ["bluetoothctl"]
		self.log(2, "Exec: " + str(p1_args) + " | " + str(p2_args))
		p1 = subprocess.Popen(p1_args, stdout=subprocess.PIPE)
		p2 = subprocess.Popen(p2_args,
				stdin=p1.stdout,
				stdout=subprocess.PIPE)
		p1.stdout.close()
		p2.communicate()


if __name__ == "__main__":
	desc = "Resolve services and reset sink profile " \
			"on Bluetooth device connect"
	parser = argparse.ArgumentParser(description=desc)
	parser.add_argument("mac", metavar="BT-MAC",
			help="Bluetooth device address")
	parser.add_argument("-p", "--profile", metavar="PROFILE",
			help="Sink profile name",
			default="a2dp_sink")
	parser.add_argument("-c", "--connect",
			help="Connect/resolve device services and exit.",
			action="store_true")
	parser.add_argument("-d", "--disconnect",
			help="Disconnect device and exit.",
			action="store_true")
	parser.add_argument("-s", "--set-profile",
			help="Set PA profile and exit.",
			action="store_true")
	parser.add_argument("-C", "--check",
			help="Check PulseAudio status.",
			action="store_true")
	parser.add_argument("-F", "--fix",
			help="Automagically try to fix issues.",
			action="store_true")
	parser.add_argument("-v", "--verbose",
			help="Be verbose. Can be given multiple times.",
			default=0, action="count")
	args = parser.parse_args()

	pabt = PABT(args.mac, profile=args.profile, verbose=args.verbose)

	if args.fix:
		if args.verbose == 0:
			pabt.verbose = 1
		PABTFixer(pabt).fix()
		sys.exit(0)
	if args.check:
		if args.verbose == 0:
			pabt.verbose = 1
		PABTChecker(pabt).check()
		sys.exit(0)
	if args.disconnect:
		pabt.disconnect_bluetooth()
	if args.connect:
		pabt.connect_bluetooth()
	if args.set_profile:
		pabt.pulseaudio_set_card_profile()
	if args.connect or args.disconnect or args.set_profile:
		sys.exit(0)

	pabt.run()
