# vim: noet ai ts=4 sts=4 sw=4

import time
from checker import PABTChecker

class PABTFixer:
	def __init__(self, pabt):
		self.pabt = pabt
		self.checker = PABTChecker(self.pabt)
		self.log = pabt.log

	def fix(self):
		self.log(2, "Trying to fix found issues")
		if not self.fix_card_connection():
			self.log(1, "Failed to fix card")
			return False
		if not self.fix_card_profile():
			self.log(1, "Failed to fix card profile")
			return False

	def fix_card_connection(self):
		if self.checker.check_card():
			return True
		self.log(1, "Trying to connect to device")
		for _ in range(10):
			self.pabt.connect_bluetooth()
			time.sleep(2)
			if self.checker.check_card():
				return True
		return False

	def fix_card_profile(self):
		card = self.checker.find_card()
		if not self.fix_card_available_profiles(card):
			log(1, "Failed to discover selected profile")
			return False
		if not self.fix_set_card_profile():
			log(1, "Failed to select profile")
			return False
		return True

	def fix_card_available_profiles(self, card):
		if self.checker.check_available_profiles(card):
			return True
		for _ in range(3):
			self.log(1, "Trying to discover selected profile")
			self.pabt.disconnect_bluetooth()
			for __ in range(10):
				time.sleep(1)
				if not self.checker.check_card():
					break
			if self.checker.check_card():
				log(1, "Failed to disconnect")
				continue

			self.pabt.connect_bluetooth()
			for __ in range(10):
				time.sleep(1)
				if self.checker.check_card():
					break
			if not self.checker.check_card():
				log(1, "Failed to connect")
				continue
			return True
		return False

	def fix_set_card_profile(self):
		if self.checker.check_active_profile(self.checker.find_card()):
			return True
		for _ in range(3):
			self.log(1, "Trying to set profile")
			self.pabt.pulseaudio_set_card_profile()
			time.sleep(1)
			card = self.checker.find_card()
			if card is None:
				continue
			if self.checker.check_active_profile(card):
				return True
		return False
