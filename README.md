# pabt

pabt is a convenience script to automatically connect to a Bluetooth audio
device and set it up with PulseAudio. It targets modern GNU/Linux desktop
setups with minimal dependencies to bloated desktop environments such as GNOME
and KDE.


## Quick start

```sh
% python3 pabt.py BT-MAC
```

This will launch pabt with BT-MAC and `a2dp_sink`.


## Getting started

### Source

pabt is hosted at GitLab, https://gitlab.com/tsaviran/pabt .

### Dependencies

pabt depends on PulseAudio, Python 3, dbus, pydbus, and whatever support files
required by those and your audio device profile.

#### Debian buster

```sh
% sudo apt install bluez python3-pydbus
```

...or that's what I wrote in my notes back in 2016.

### Set up

This section is written based on assumptions. None of this has been tested on a
fresh system.

pabt expects the devices to be paired and trusted. You can use `bluetoothctl`
to discover, pair, and trust a device.

```sh
% bluetoothctl
Agent registered
[device]# scan on
[NEW] Device 01:23:45:67:89:ab Headphones
[device]# connect 01:23:45:67:89:ab
[device]# trust 01:23:45:67:89:ab
```


## Usage

```
usage: pabt.py [-h] [-p PROFILE] [-c] [-d] [-s] [-v] BT-MAC

Resolve services and reset sink profile on Bluetooth device connect

positional arguments:
  BT-MAC                Bluetooth device address

optional arguments:
  -h, --help            show this help message and exit
  -p PROFILE, --profile PROFILE
                        Sink profile name
  -c, --connect         Connect/resolve device services and exit.
  -d, --disconnect      Disconnect device and exit.
  -s, --set-profile     Set PA profile and exit.
  -v, --verbose         Be verbose
```


## Known issues

All issues encountered by the developer to date can be traced to Bluetooth
service resolution or setting the profile with PulseAudio. This is no
surprise - service resolution is the very core of what pabt does and depends
on. pabt can be asked to try to fix known issues should the automation fail.

```sh
% pabt --fix 01:23:45:67:89:ab
```

If disconnecting/connecting the device is slow, --fix may not work for you.
When --fix doesn't work for your device(s), you may try to reconnect manually
(whilst pabt is running).

```sh
% pabt --disconnect 01:23:45:67:89:ab
# Wait for a few seconds, or until your audio device acknowledges
# it has been disconnected.
% pabt --connect 01:23:45:67:89:ab
```

### Output device is not connected

If pabt does not receive a service resolution event from D-Bus, it will not set
up the device. This may happen if you've powered on your Bluetooth device when
pabt was not running, or when service resolution fails. To address this, you
can have pabt manually connect to the device. This also triggers service
resolution.

```sh
% pabt --connect 01:23:45:67:89:ab
```

### Failed to set card profile to 'a2dp_sink'

If your device supports A2DP, service resolution failed to discover A2DP. You
may want to reconnect to try again.

```sh
% pabt --fix 01:23:45:67:89:ab
```

### Poor audio quality

Poor audio quality is likely a result of PulseAudio failing to set profile to
A2DP. It is possible not all Bluetooth services were resolved. If your device
supports A2DP (and it probably does), disconnecting and reconnecting is likely
to help.

```sh
% pabt --fix 01:23:45:67:89:ab
```

### Audio stops, clients remain connected

Also: New clients fail to connect

Developer has observed this twice. The second time it happened, PA showed all
clients still connected, but BlueZ sink was suspended. Physically switching the
Bluetooth device off and back on (and having pabt reconnect) solved the issue.
A simple reconnect might also fix this.

```sh
% pabt --disconnect 01:23:45:67:89:ab
# Wait for a few seconds, or until your audio device acknowledges
# it has been disconnected.
% pabt --connect 01:23:45:67:89:ab
```
